package com.dhirajmore.logindemo.Controllers;

import com.dhirajmore.logindemo.Entity.CustomerSignUp;
import com.dhirajmore.logindemo.Service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class CustomerController {
    @Autowired
    CustomerService customerService;
    @GetMapping("/login")
    public String home() {
        return "login";
    }
    @PostMapping("/signin")
    public CustomerSignUp login(@RequestParam("username") String username, @RequestParam("password") String password){

        return customerService.login(username,password);
    }
    @GetMapping("/sign")
    public String homepage() {
        return "signup";
    }

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }
}
