package com.dhirajmore.logindemo.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LocControllers {
    @GetMapping("/location")
    public String loc() {
        return "loc";
    }
}
