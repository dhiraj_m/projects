package com.dhirajmore.logindemo.Entity;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "address")
@Entity
public class Address {
    private long id;
    private String name;
    @Id
    private String mobile;
    private String pincode;
    private String city;
    private String addr;

    @Override
    public String toString() {
        return "Address{" +
                "name='" + name + '\'' +
                ", mobile='" + mobile + '\'' +
                ", pincode='" + pincode + '\'' +
                ", city='" + city + '\'' +
                ", addr='" + addr + '\'' +
                '}';
    }
}
