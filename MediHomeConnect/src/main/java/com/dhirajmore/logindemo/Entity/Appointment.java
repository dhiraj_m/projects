package com.dhirajmore.logindemo.Entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "appointment")
@Entity
public class Appointment {
    @Id
    private long id;
    private String contact;
    private long status;

    @Override
    public String toString() {
        return "Appointment{" +
                "id=" + id +
                ", contact='" + contact + '\'' +
                ", status=" + status +
                '}';
    }
}



