package com.dhirajmore.logindemo.Entity;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "patients")
@Entity
public class CustomerSignUp {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;
    private String name;

    private String email;
    private String mobile;

    @Override
    public String toString() {
        return "CustomerSignUp{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", mobile='" + mobile + '\'' +
                ", password='" + password + '\'' +
                ", conpass='" + conpass + '\'' +
                '}';
    }


    private String password;

    private String conpass;

    public CustomerSignUp(String name, String email, String mobile, String password, String conpass) {
        this.name = name;
        this.email = email;
        this.mobile = mobile;
        this.password = password;
        this.conpass = conpass;
    }

}
