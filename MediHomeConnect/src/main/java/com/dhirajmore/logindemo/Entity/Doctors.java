package com.dhirajmore.logindemo.Entity;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Table(name = "doctorslist")
@NoArgsConstructor
public class Doctors {

    @Id
    private String name;
    private Double lat;
    private Double longi;
    private String contact;
    private Long exp;
    private String education;
    private String spec;

}
