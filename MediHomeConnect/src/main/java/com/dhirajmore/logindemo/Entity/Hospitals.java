package com.dhirajmore.logindemo.Entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Table(name = "hospitallist")
@NoArgsConstructor
public class Hospitals {
    private  String address = null;
    @Id
    private int pincode = 414005;

    public Hospitals(String address,int pincode) {
        this.address = address;
        this.pincode = pincode;
    }

    @Override
    public String toString() {
        return "Hospitals{" +
                "address='" + address + '\'' +
                ", pincode='" + pincode + '\'' +
                '}';
    }
}
