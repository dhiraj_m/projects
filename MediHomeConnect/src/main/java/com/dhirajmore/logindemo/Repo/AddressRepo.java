package com.dhirajmore.logindemo.Repo;

import com.dhirajmore.logindemo.Entity.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepo extends JpaRepository<Address,Double> {
    Address findById(long id);
}
