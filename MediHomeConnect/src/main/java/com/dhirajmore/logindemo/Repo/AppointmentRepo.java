package com.dhirajmore.logindemo.Repo;

import com.dhirajmore.logindemo.Entity.Appointment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AppointmentRepo extends JpaRepository<Appointment,Double> {
        public Appointment findByContact(String contact);
}
