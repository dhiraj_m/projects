package com.dhirajmore.logindemo.Repo;

import com.dhirajmore.logindemo.Entity.Doctors;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DoctorsRepo extends JpaRepository<Doctors,Long> {

    List<Doctors> findAll();

    Doctors findByContact(String contact);
}



























