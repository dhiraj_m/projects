package com.dhirajmore.logindemo.Repo;

import com.dhirajmore.logindemo.Entity.Hospitals;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface HospitalRepo extends JpaRepository<Hospitals,Long> {

    List<Hospitals> findByPincode(int pincode);
}
