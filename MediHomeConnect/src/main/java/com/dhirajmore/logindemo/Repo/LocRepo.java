package com.dhirajmore.logindemo.Repo;

import com.dhirajmore.logindemo.Entity.LocationData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocRepo extends JpaRepository<LocationData,Double> {

}
