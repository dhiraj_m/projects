package com.dhirajmore.logindemo.Rest;


import com.dhirajmore.logindemo.Entity.Address;
import com.dhirajmore.logindemo.Repo.AddressRepo;
import com.dhirajmore.logindemo.Service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class AddressController {
    @Autowired
    AddressService addressService;

    public AddressController(AddressService addressService) {
        this.addressService = addressService;
    }



    @PostMapping("/saveAddress")
    public ResponseEntity<Address> saveAddress(@RequestBody Address address) {
        Address address1 = addressService.saveAdd(address);
        if (address1 != null) {
            return ResponseEntity.ok(address1);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }
    @PostMapping("/patientAddress")
    public ResponseEntity<Address> getPatientAddress(@RequestBody Map<String,Long> id) {
        long id2 = id.get("id");
        Address address = addressService.getPatientAddress(id2);
        System.out.println(address);
        if (address != null) {
            return ResponseEntity.ok(address);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }
}
