package com.dhirajmore.logindemo.Rest;

import com.dhirajmore.logindemo.Entity.Address;
import com.dhirajmore.logindemo.Entity.Appointment;
import com.dhirajmore.logindemo.Repo.AddressRepo;
import com.dhirajmore.logindemo.Service.AppointmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class AppointmentController {
    @Autowired
    AppointmentService appointmentService;
//    @Autowired
//    AddressRepo addressRepo;
//
//    public AppointmentController(AddressRepo addressRepo) {
//        this.addressRepo = addressRepo;
//    }

    public AppointmentController(AppointmentService appointmentService) {
        this.appointmentService = appointmentService;
    }
    @PostMapping("/sendRequest")
    public void sendReq(@RequestBody Appointment appointment) {
        appointmentService.saveReq(appointment);
    }
    @PostMapping("/show")
    public ResponseEntity<Appointment> show(@RequestBody Map<String,String> contact) {
        String cont = contact.get("contact");
        Appointment appointment = appointmentService.show(cont);
//        Address address = addressRepo.findById(appointment.getId());
        if (appointment != null) {
            return ResponseEntity.ok(appointment);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }
}
