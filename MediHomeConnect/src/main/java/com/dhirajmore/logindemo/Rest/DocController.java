package com.dhirajmore.logindemo.Rest;

import com.dhirajmore.logindemo.Entity.Appointment;
import com.dhirajmore.logindemo.Entity.Doctors;
import com.dhirajmore.logindemo.Service.DoctorsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.Map;

import java.awt.geom.Point2D;
import java.util.List;

@RestController
public class DocController {
    @Autowired
    DoctorsService doctorsService;

    public DocController(DoctorsService doctorsService) {
        this.doctorsService = doctorsService;
    }

    @PostMapping("/getDoctors")
    public ResponseEntity<List<Doctors>> getDoc(@RequestBody Map<String,Double> coordinates) {
        Double lat = coordinates.get("latitude");
        Double longi = coordinates.get("longitude");
        System.out.println(lat);

        List<Doctors> doctorsList = doctorsService.getDoc(lat,longi);

        if (doctorsList != null) {
            System.out.println(doctorsList);
            System.out.println("THereeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
            return ResponseEntity.ok(doctorsList);
        } else {
            System.out.println("Hereeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        //return doctorsService.getDoc(lat,longi);
    }
    @PostMapping("/docLogin")
    public ResponseEntity<Doctors> getDocLog(@RequestBody Map<String,String> credentials) {
        String contact = credentials.get("mobile");
        String password = credentials.get("password");

        System.out.println(contact);
        System.out.println("contact");


        Doctors doctors = doctorsService.login(contact,password);
        if (doctors != null) {
            return ResponseEntity.ok(doctors);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

}
