package com.dhirajmore.logindemo.Rest;

import com.dhirajmore.logindemo.Entity.Doctors;
import com.dhirajmore.logindemo.Entity.LocationData;
import com.dhirajmore.logindemo.Service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class LocationController {

    @Autowired
    LocationService locationService;

    public LocationController(LocationService locationService) {
        this.locationService = locationService;
    }

    @PostMapping("/getLocation")
    public ResponseEntity<String> saveLocation(@RequestBody LocationData locationData) {

        return locationService.saveLoc(locationData);
    }
}

//AIzaSyCnRSbg-jfhGBuRlH2-_d6aKMRJ_gbbqgY
