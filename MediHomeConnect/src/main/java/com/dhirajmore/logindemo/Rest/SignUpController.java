package com.dhirajmore.logindemo.Rest;

import com.dhirajmore.logindemo.Entity.Address;
import com.dhirajmore.logindemo.Repo.AddressRepo;
import com.dhirajmore.logindemo.Service.CustomerService;
import com.dhirajmore.logindemo.Entity.CustomerSignUp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
//@ComponentScan(basePackages = {"logindemo"})
public class SignUpController {
    @Autowired
    CustomerService customerService;


    public SignUpController(CustomerService customerService) {
        this.customerService = customerService;
    }
    @PostMapping("/login")
    public ResponseEntity<CustomerSignUp> login(@RequestBody Map<String, String> credentials) {
        String username = credentials.get("username");
        String password = credentials.get("password");

        CustomerSignUp customerSignUp = customerService.login(username, password);


        if (customerSignUp != null) {
            return ResponseEntity.ok(customerSignUp);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    @PostMapping("/add")
    public CustomerSignUp addCustomer(@RequestBody CustomerSignUp customerSignUp) {

        System.out.println(customerSignUp);
        return customerService.addCustomer(customerSignUp);
        //System.out.println(customerSignUp);
        //return "jsonData";
    }
    @PostMapping("/patRequest")
    public ResponseEntity<CustomerSignUp> patRequest(@RequestBody Map<String,Long> id) {
        long id1 = id.get("id");
        CustomerSignUp customerSignUp = customerService.patientReq(id1);
        if (customerSignUp != null) {
            return ResponseEntity.ok(customerSignUp);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }
}
