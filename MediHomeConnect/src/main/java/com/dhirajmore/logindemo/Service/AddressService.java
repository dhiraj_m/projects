package com.dhirajmore.logindemo.Service;

import com.dhirajmore.logindemo.Entity.Address;
import com.dhirajmore.logindemo.Entity.CustomerSignUp;
import com.dhirajmore.logindemo.Repo.AddressRepo;
import com.dhirajmore.logindemo.Repo.CustomerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddressService {
    @Autowired
    AddressRepo addressRepo;

    CustomerSignUp customerSignUp;
    public void getUser(String username, CustomerRepo customerRepo) {
        customerSignUp = customerRepo.findByEmail(username);
    }

    public AddressService(AddressRepo addressRepo) {
        this.addressRepo = addressRepo;
    }

    public Address saveAdd(Address address) {
//        customerSignUp.getAddresses().add(address);
        System.out.println(customerSignUp);
        return addressRepo.save(address);
    }

    public Address getPatientAddress(long id) {
        return addressRepo.findById(id);
    }
}
