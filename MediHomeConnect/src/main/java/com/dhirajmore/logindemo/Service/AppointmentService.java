package com.dhirajmore.logindemo.Service;

import com.dhirajmore.logindemo.Entity.Appointment;
import com.dhirajmore.logindemo.Repo.AppointmentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AppointmentService {
    @Autowired
    AppointmentRepo appointmentRepo;

    public AppointmentService(AppointmentRepo appointmentRepo) {
        this.appointmentRepo = appointmentRepo;
    }

    public void saveReq(Appointment appointment) {
        System.out.println("saveddddddddddddddddddddddddddddddd");
        appointmentRepo.save(appointment);
    }

    public Appointment show(String cont) {
        Appointment appointment = appointmentRepo.findByContact(cont);
        if(appointment != null)
            return appointment;
        return null;
    }
}
