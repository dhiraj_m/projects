package com.dhirajmore.logindemo.Service;

import ch.qos.logback.classic.pattern.SyslogStartConverter;
import com.dhirajmore.logindemo.Repo.CustomerRepo;
import com.dhirajmore.logindemo.Entity.CustomerSignUp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService {

    @Autowired
    CustomerRepo customerRepo;
    @Autowired
    AddressService addressService;

    public CustomerService(CustomerRepo customerRepo, AddressService addressService) {
        this.customerRepo = customerRepo;
        this.addressService = addressService;
    }

    public CustomerSignUp addCustomer(CustomerSignUp customerSignUp) {
      if(customerRepo.findByEmail(customerSignUp.getEmail()) != null) {
          return null;
      }
      customerRepo.save(customerSignUp);
      return customerSignUp;
    }

    public CustomerSignUp login(String username, String password) {
        System.out.println("hello");
        CustomerSignUp customerSignUp = customerRepo.findByEmail(username);

        if(customerSignUp == null) return null;

        if(customerSignUp.getPassword().equals(password)) {
            addressService.getUser(username,customerRepo);
            return customerSignUp;
        }

        return null;
    }


    public CustomerSignUp patientReq(long id1) {
        CustomerSignUp customerSignUp = customerRepo.findById(id1);
        if(customerSignUp != null)
            return customerSignUp;
        return null;
    }
}
