package com.dhirajmore.logindemo.Service;

import com.dhirajmore.logindemo.Entity.Appointment;
import com.dhirajmore.logindemo.Entity.Doctors;
import com.dhirajmore.logindemo.Repo.DoctorsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

@Service
public class DoctorsService {
    @Autowired
    DoctorsRepo doctorsRepo;
    public void getDrList() {

    }
    public List<Doctors> getDoc(Double lat, Double longi) {
        Point2D.Double endCoordinates = calculateCoordinates(lat,longi,10);
        List<Doctors> temp = doctorsRepo.findAll();

        List<Doctors> doctorsList = new ArrayList<Doctors>();

        for(int i=0 ;i< temp.size(); i++) {
            System.out.println("Exception");
            if(lat <= temp.get(i).getLat() && temp.get(i).getLat() <= endCoordinates.x ) {
                if(longi <= temp.get(i).getLongi() && temp.get(i).getLongi() <= endCoordinates.y ) {
                    doctorsList.add(temp.get(i));
                }
            }
        }
        return doctorsList;
    }
    public static Point2D.Double calculateCoordinates(Double latitude, Double longitude, double distanceKm) {
        // Earth radius in kilometers
        final double earthRadiusKm = 6371.0;

        // Convert latitude and longitude from degrees to radians
        double lat1 = Math.toRadians(latitude);
        double lon1 = Math.toRadians(longitude);

        // Calculate the angular distance covered by the distanceKm
        double angularDistance = distanceKm / earthRadiusKm;

        // Calculate new latitude and longitude
        double lat2 = lat1 + angularDistance;
        double lon2 = lon1 + angularDistance / Math.cos(lat1);

        // Convert back to degrees
        lat2 = Math.toDegrees(lat2);
        lon2 = Math.toDegrees(lon2);

        return new Point2D.Double(lat2, lon2);
    }
    public Doctors login(String contact,String password) {
        Doctors doctors = doctorsRepo.findByContact(contact);

        if(password.equals("123"))
            return doctors;
        return null;
    }
}
