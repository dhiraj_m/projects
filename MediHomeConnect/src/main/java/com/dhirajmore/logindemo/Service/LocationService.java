package com.dhirajmore.logindemo.Service;

import com.dhirajmore.logindemo.Entity.Doctors;
import com.dhirajmore.logindemo.Entity.LocationData;
import com.dhirajmore.logindemo.Repo.LocRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.awt.geom.Point2D;
import java.util.List;

@Service
public class LocationService {
    @Autowired
    LocRepo locRepo;

    public LocationService(LocRepo locRepo) {
        this.locRepo = locRepo;
    }

    public ResponseEntity<String> saveLoc(LocationData locationData) {
        locRepo.save(locationData);
        return ResponseEntity.ok("Location Saved Successfully");
    }

}
