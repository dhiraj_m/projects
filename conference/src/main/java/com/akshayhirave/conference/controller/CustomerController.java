package com.akshayhirave.conference.controller;

import com.akshayhirave.conference.entity.CustomerLogin;
import com.akshayhirave.conference.entity.PublishPaper;
import com.akshayhirave.conference.service.CustomerService;
import com.akshayhirave.conference.entity.CustomerSignUp;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@AllArgsConstructor
@RestController
public class CustomerController {

    private final CustomerService customerService;

    @PostMapping("/signup")
    public CustomerSignUp signUpCustomer(@RequestBody CustomerSignUp customerSignUp) {
        return customerService.signUpCustomer(customerSignUp);
    }

    @PostMapping("/login")
    public CustomerSignUp loginCustomer(@RequestBody CustomerLogin customerLogin) {
        return customerService.loginCustomer(customerLogin);
    }
    @GetMapping("/getCustomer/{customerEmail}")
    private CustomerSignUp getCustomer(@PathVariable(name = "customerEmail") String customerEmail) {
        return customerService.getCustomer(customerEmail);
    }
}
