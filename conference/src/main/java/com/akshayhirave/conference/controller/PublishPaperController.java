package com.akshayhirave.conference.controller;

import com.akshayhirave.conference.entity.CustomerSignUp;
import com.akshayhirave.conference.entity.PublishPaper;
import com.akshayhirave.conference.service.PublishPaperService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.List;

@RestController
@AllArgsConstructor
public class PublishPaperController {

    private final PublishPaperService publishPaperService;

    @PostMapping("/addPublishPaper/{conferenceId}/{authorId}")
    private Long addPublishPaper(@RequestBody PublishPaper publishPaper,
                                   @PathVariable(name = "conferenceId") Long conferenceId,
                                 @PathVariable(name = "authorId")Long authorId) {
        return publishPaperService.addPublishPaper(publishPaper, conferenceId, authorId);
    }

    @PostMapping("/addPdf/{publishPaperId}")
    private String addPdf(@RequestPart(name = "pdf") MultipartFile pdf,
                          @PathVariable(name = "publishPaperId") Long publishPaperId) throws IOException {
        return publishPaperService.addPdf(pdf, publishPaperId);
    }

    @GetMapping("/getPdf/{publishPaperId}")
    private ResponseEntity<byte[]> downloadPdf(@PathVariable(name = "publishPaperId") Long publishPaperId) {
        return publishPaperService.downloadPdf(publishPaperId);
    }
    @GetMapping("/getSubmission/{conferenceId}")
    private ResponseEntity<List<PublishPaper>> getSubmission(@PathVariable(name = "conferenceId") Long conferenceId) {
        return publishPaperService.getSubmission(conferenceId);
    }
    @GetMapping("/getReviewPapers/{reviewerId}")
    private ResponseEntity<List<PublishPaper>> getReviewPapers(@PathVariable(name = "reviewerId") Long reviewerId) {
        return publishPaperService.getReviewPapers(reviewerId);
    }

    @GetMapping("/getAuthorPapers/{authorId}")
    private ResponseEntity<List<PublishPaper>> getAuthorPapers(@PathVariable(name = "authorId") Long authorId) {
        return publishPaperService.getAuthorPapers(authorId);
    }

    @GetMapping("/getPaper/{publishPaperId}")
    private ResponseEntity<PublishPaper> getPaper(@PathVariable(name = "publishPaperId") Long publishPaperId) {
        return publishPaperService.getPaper(publishPaperId);
    }

    @PostMapping("/setReviewer/{publishPaperId}/{reviewerId}")
    private ResponseEntity<PublishPaper> setReviewer(@PathVariable(name = "publishPaperId") Long publishPaperId,
                                                     @PathVariable(name = "reviewerId") Long reviewerId) {
        return publishPaperService.setReviewer(publishPaperId, reviewerId);
    }
    @PostMapping("/updateReviewStatus/{publishPaperId}/{reviewStatus}/{feed}")
    private ResponseEntity<PublishPaper> updateReviewStatus(@PathVariable(name = "publishPaperId") Long publishPaperId,
                                                     @PathVariable(name = "reviewStatus") String reviewStatus,
                                                            @PathVariable(name= "feed") String feed) {
        return publishPaperService.updateReviewStatus(publishPaperId, reviewStatus, feed);
    }
}
