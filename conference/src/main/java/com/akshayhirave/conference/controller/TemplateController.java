package com.akshayhirave.conference.controller;

import com.akshayhirave.conference.entity.Conference;
import com.akshayhirave.conference.repository.ConferenceRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class TemplateController {
    @GetMapping
    public String showHomePage() {
        return "homepage";
    }
    @GetMapping("/login")
    public String showLoginPage() {
        return "login";
    }
    @GetMapping("/signup")
    public String showSignupPage() {
        return "signup";
    }
    @GetMapping("/dashboard")
    public String showDashboard() {
        return "dashboard";
    }
    @GetMapping("/easychair")
    public String showEasyChair() {
        return "easychair";
    }
    @GetMapping("/myconference")
    public String showMyConference() {
        return "myconference";
    }
    @GetMapping("/createnewconference")
    public String showCreateNewConference() {
        return "createnewconference";
    }
    @GetMapping("/addreviewer")
    public String showAddReviewer() { return "AddReviewer";}
    @GetMapping("/preprintform")
    public String showPrePrintForm() { return "preprintform";}
    @GetMapping("/reviewstatus")
    public String showReviewStatus() { return "reviewStatus";}
    @GetMapping("/myrecentrole")
    public String showMyRecentRole() { return "myrecentrole";}
    @GetMapping("/reviewer")
    public String showReviewer() { return "reviewer";}
    @GetMapping("/updatestatus")
    public String showUpdateStatus() { return "updatestatus";}
    @GetMapping("/author")
    public String showAuthor() { return "author";}
    @GetMapping("/submissions/{conferenceId}")
    public String showSubmissions(@PathVariable(name = "conferenceId") Long conferenceId, Model model) {
        model.addAttribute("conferenceId", conferenceId);
        return "submissions";
    }

}
