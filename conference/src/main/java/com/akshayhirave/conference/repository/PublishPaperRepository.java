package com.akshayhirave.conference.repository;

import com.akshayhirave.conference.entity.CustomerSignUp;
import com.akshayhirave.conference.entity.PublishPaper;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PublishPaperRepository extends JpaRepository<PublishPaper, Long> {
    @Query(value = "select p from PublishPaper p where p.reviewer.id = ?1")
    List<PublishPaper> getReviewPapers(Long reviewerId);
    @Query(value = "select p from PublishPaper p where p.conference.conferenceId = ?1")
    List<PublishPaper> getSubmission(Long conferenceId);

    @Query(value = "select p from PublishPaper p where p.publishPaperId = ?1")
    PublishPaper getPaper(Long publishPaperId);
    @Query(value = "select p from PublishPaper p where p.author.id = ?1")
    List<PublishPaper> getAuthorPapers(Long authorId);
}
