package com.akshayhirave.conference.service;

import com.akshayhirave.conference.entity.Conference;
import com.akshayhirave.conference.entity.CustomerSignUp;
import com.akshayhirave.conference.entity.PublishPaper;
import com.akshayhirave.conference.repository.ConferenceRepository;
import com.akshayhirave.conference.repository.CustomerRepo;
import com.akshayhirave.conference.repository.PublishPaperRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.List;

@Service
@AllArgsConstructor
public class PublishPaperService {

    private final PublishPaperRepository publishPaperRepository;
    private final ConferenceRepository conferenceRepository;
    private final CustomerRepo customerRepo;
    private final EmailService emailService;

    public Long addPublishPaper(PublishPaper publishPaper, Long conferenceId, Long authorId) {

        Conference conference = conferenceRepository.getById(conferenceId);
        publishPaper.setConference(conference);

        CustomerSignUp customerSignUp = customerRepo.findById(authorId).get();

        publishPaper.setAuthor(customerSignUp);
        publishPaperRepository.save(publishPaper);
        emailService.sendSimpleMessage(customerSignUp.getEmail(),"Paper Submission Confirmation: ".concat(publishPaper.getTitle()),"Dear "+customerSignUp.getFirstName()+",\n" +
                "\n" +
                "I hope this email finds you well. I am writing to confirm the successful submission of your paper titled \""+publishPaper.getTitle()+"\" for the upcoming "+conference.getConferenceTitle()+".\n" +
                "\n" +
                "We have received your submission and it is currently being reviewed by our team. You will be notified of the review outcome soon. In the meantime, if you have any questions or need further information, please do not hesitate to contact me.\n" +
                "\n" +
                "Thank you for your contribution to our conference. We look forward to your participation and hope to see you at the event.\n" +
                "\n" +
                "Best regards,\n" +
                conference.getConferenceTitle()+" Organizing Committee");

        return publishPaper.getPublishPaperId();
    }

    public String addPdf(MultipartFile pdf, Long publishPaperId) throws IOException {

        PublishPaper publishPaper = publishPaperRepository.getById(publishPaperId);
        publishPaper.setPaper(pdf.getBytes());
        publishPaperRepository.save(publishPaper);
        return "pdf added";
    }

    @Transactional
    public ResponseEntity<byte[]> downloadPdf(Long publishPaperId) {

        byte[] pdfContent = publishPaperRepository.getById(publishPaperId).getPaper();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_PDF);
        String fileName = publishPaperRepository.getById(publishPaperId).getTitle();
        headers.setContentDispositionFormData("attachment", fileName+".pdf");

        return new ResponseEntity<>(pdfContent, headers, HttpStatus.OK);
    }
    @Transactional
    public ResponseEntity<List<PublishPaper>> getSubmission(Long conferenceId) {

        return ResponseEntity.ok(publishPaperRepository.getSubmission(conferenceId));
    }

    public ResponseEntity<PublishPaper> getPaper(Long publishPaperId) {

        return ResponseEntity.ok(publishPaperRepository.getPaper(publishPaperId));
    }

    public ResponseEntity<PublishPaper> setReviewer(Long publishPaperId, Long reviewerId) {

        PublishPaper publishPaper = publishPaperRepository.findById(publishPaperId).get();
        CustomerSignUp customerSignUp = customerRepo.findById(reviewerId).get();

        publishPaper.setReviewer(customerSignUp);
        publishPaperRepository.save(publishPaper);
        emailService.sendSimpleMessage(customerSignUp.getEmail(),"Review Assignment:".concat(publishPaperRepository.getPaper(publishPaperId).getTitle()),"Dear "+customerSignUp.getFirstName()+",\n" +
                "\n" +
                "I hope this email finds you well. My name is "+ publishPaperRepository.getPaper(publishPaperId).getConference().getCustomerSignUp().getFirstName() +" and I am writing to you on behalf of the organizing committee of the "+publishPaperRepository.getPaper(publishPaperId).getConference().getConferenceTitle()+".\n\n" + "We are pleased to inform you that you have been assigned as a reviewer for one of the paper under this conference. You will find the details regarding this in reviewer section of ConfTable platform.\n\n"+"Thank you for your participation in the conference. We greatly appreciate your contribution to the peer-review process.\n" +
                "\n" +
                "Best regards,\n" +
                publishPaperRepository.getPaper(publishPaperId).getConference().getConferenceTitle()+" Organizing Committee");
        return ResponseEntity.ok(publishPaper);
    }
    @Transactional
    public ResponseEntity<List<PublishPaper>> getReviewPapers(Long reviewerId) {
        return ResponseEntity.ok(publishPaperRepository.getReviewPapers(reviewerId));
    }

    public ResponseEntity<PublishPaper> updateReviewStatus(Long publishPaperId, String reviewStatus, String feedback) {

        PublishPaper publishPaper = publishPaperRepository.findById(publishPaperId).get();

        publishPaper.setReviewStatus(PublishPaper.ReviewStatus.valueOf(reviewStatus));
        publishPaper.setFeedback(feedback);
        publishPaperRepository.save(publishPaper);
        return ResponseEntity.ok(publishPaper);
    }
    @Transactional
    public ResponseEntity<List<PublishPaper>> getAuthorPapers(Long authorId) {
        return ResponseEntity.ok(publishPaperRepository.getAuthorPapers(authorId));
    }
}
