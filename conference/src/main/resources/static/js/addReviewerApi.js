let registrationForm = document.getElementById("addReviewer");
if (registrationForm) {
  registrationForm.addEventListener('submit', function (event) {
    event.preventDefault(); // Prevent the form from submitting

    let publishPaperId = document.getElementById("publishPaperId").value;
    let customerEmail = document.getElementById("email").value;

    fetch(`http://localhost:8080/getCustomer/${customerEmail}`)
      .then(response => {
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        return response.json();
      })
      .then(data => {
        if (!data) {
          throw new Error('Empty response or invalid JSON');
        }

        let reviewerId = data.id;

        fetch(`http://localhost:8080/setReviewer/${publishPaperId}/${reviewerId}`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          }
        })
        .then(response => {
          if (!response.ok) {
            throw new Error('Network response was not ok');
          }
          return response.json();
        })
        .then(data => {
          let conferenceId = sessionStorage.getItem('conferenceId')
          window.location.href = `http://localhost:8080/submissions/${conferenceId}`;
        })
        .catch(error => {
          console.error('There was a problem with your setReviewer fetch operation:', error);
        });
      })
      .catch(error => {
        console.error('There was a problem with your getCustomer fetch operation:', error);
        let h3 = document.createElement("h3");
        h3.innerHTML = "Reviewer Not Found";
        h3.style.color = "red";
        h3.style.position = "fixed";
        h3.style.left = "50%";
        h3.style.transform = "translate(-50%, -50%)";
        document.body.append(h3);

      });
  });
}
let status = document.getElementById("status");

status.onclick = ()=> {
    window.location.href = "http://localhost:8080/reviewstatus";
}
