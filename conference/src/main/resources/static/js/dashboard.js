var user = JSON.parse(sessionStorage.getItem('user'));
if(user == null) {
    window.location.href = "/"
}

let userInitial = document.getElementById("userInitial");
let email = user.email;
userInitial.innerHTML = email[0].toUpperCase();

let EasyChair = document.getElementById("EasyChair");
if(EasyChair != null) {
    EasyChair.onclick = ()=> {
        window.location.href = "/easychair"
    }
}

let preprints = document.getElementById("preprints");
if(preprints != null) {
    preprints.onclick = ()=> {
        window.location.href = "/preprintform"
    }
}

