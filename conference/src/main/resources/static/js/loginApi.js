function showDashboard(data) {
  sessionStorage.setItem('user', JSON.stringify(data));
  window.location.href = '/dashboard';  // Redirect to the login page
}
var registrationForm = document.getElementById('loginForm');

// Adding an event listener for the "submit" event
if (registrationForm) {
  registrationForm.addEventListener('submit', function (event) {
    // Prevent the default form submission behavior
    event.preventDefault();

    // Get the data from the input fields
    const email = document.getElementById('email').value;
    const password = document.getElementById('password').value;

    // URL of your API endpoint running locally
    const apiUrl = 'http://localhost:8080/login'; // Replace with your API endpoint URL

    // Making a POST request to the API endpoint using Fetch API
    fetch(apiUrl, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ "email": email, "password": password }),
    })
      .then(response => {
        // Check if the request was successful
        if (!response.ok) {
          throw new Error('Invalid credentials'); // or handle the error accordingly
        }
        // Parse the JSON response
        return response.json();
      })
      .then(data => {
        // Optionally, do something with the authenticated user data
        console.log('Login successful:', data);
        showDashboard(data);
        // Redirect or perform other actions after successful login
      })
      .catch(error => {
        // Handle and provide feedback for errors during login
        console.error('Login error:', error.message);
        let h3 = document.createElement("h3");
        h3.id = "h3";
        h3.innerHTML = "Incorrect username or password";
        h3.style.color = "red";
        h3.style.position = "fixed";
        h3.style.left = "50%";
        h3.style.top = "80%";
        h3.style.transform = "translate(-50%, -50%)";
        document.body.append(h3);
      });
  });
}
