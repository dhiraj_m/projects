var id = user.id;

fetch(`http://localhost:8080/getConference/${id}`)
    .then(response => response.json())
    .then(data => {
        const table = document.getElementById('jsonTable');
        const tbody = table.querySelector('tbody');

        // Remove existing rows
        tbody.innerHTML = '';

        if (data.length === 0) {
            table.style.display = 'none'; // Hide the table
        } else {
            data.forEach(obj => {
                const row = document.createElement('tr');
                row.innerHTML = `
                    <td>${obj.conferenceId}</td>
                    <td><a href="/submissions/${obj.conferenceId}" style="text-decoration: none;">${obj.acronym}</a></td>
                    <td>${obj.conferenceTitle}</td>
                `;
                tbody.appendChild(row);
            });

            table.style.display = 'table'; // Show the table
        }
    })
    .catch(error => {
        console.error('Error fetching data:', error);
    });
