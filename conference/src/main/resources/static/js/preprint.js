//author information
var no = 1;
document.getElementById('addAuthorLink').addEventListener('click', function(event) {
    event.preventDefault(); // Prevent default link behavior

    var authorFormsContainer = document.getElementById('authorFormsContainer');
    var lastAuthorForm = authorFormsContainer.lastElementChild;
    var newAuthorForm = lastAuthorForm.cloneNode(true);
    var index = parseInt(newAuthorForm.querySelector('h2').textContent.split(' ')[1]) + 1;
    newAuthorForm.querySelector('h2').textContent = 'Author ' + index;
    var inputs = newAuthorForm.querySelectorAll('input');
    inputs[0].setAttribute('id', 'first-name-' + index);
    inputs[0].setAttribute('name', 'first-name-' + index);
    inputs[1].setAttribute('id', 'last-name-' + index);
    inputs[1].setAttribute('name', 'last-name-' + index);
    inputs[2].setAttribute('id', 'email-' + index);
    inputs[2].setAttribute('name', 'email-' + index);
    authorFormsContainer.appendChild(newAuthorForm);
    no = index;
});

//file uploadation
document.getElementById('chooseFileBtn').addEventListener('click', function() {
  document.getElementById('fileID').click();
});

document.getElementById('fileID').addEventListener('change', function() {
  var selectedFile = this.files[0];
  document.getElementById('selectedFileName').textContent = 'Selected File: ' + selectedFile.name;
});

let submit = document.getElementById("submit");

submit.onclick = (event) => {

    event.preventDefault(); // Prevent form submission

    let conferenceId = document.getElementById("conferenceId").value;

    let authors = [];

    for(let i = 1; i<= no; i++) {
        authors.push(document.getElementById("first-name-"+i).value + " " + document.getElementById("last-name-"+i).value);
    }

    let title = document.getElementById("Title").value;
    let abstract = document.getElementById("abstract").value;
    let parea = document.getElementById("parea").value;
    let sarea = document.getElementById("sarea").value;
    let keywords = document.getElementById("keywords").value;

    var selectedFile = document.getElementById('fileID').files[0];

    var formData = new FormData();
    formData.append('pdf', selectedFile);

    let user = JSON.parse(sessionStorage.getItem("user"));
    let authorId = user.id;
    console.log(user);
    console.log(authorId);
    fetch(`http://localhost:8080/addPublishPaper/${conferenceId}/${authorId}`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({"authors":authors, "title":title, "abstractString":abstract, "keywords":keywords})
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
        return response.text();
    })
    .then(data => {
        console.log(data); // Log the response data to the console or use it as needed
        let publishPaperId = data;
        fetch(`http://localhost:8080/addPdf/${publishPaperId}`, {
            method: "POST",
            body: formData,
        })
        .then(response => {
            if (!response.ok) {
                throw new Error("Network response was not ok");
            }
            return response.text();
        })
        .then(data => {
            console.log("File uploaded successfully:", data);
            window.location.href = '/author';
        })
        .catch(error => {
            console.error("There was a problem with your fetch operation addPdf:", error);
        });
    })
    .catch(error => {
        console.error('There was a problem with your fetch operation addPublishPaper:', error);
    });

}
