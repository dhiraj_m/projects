var conferenceId = sessionStorage.getItem("conferenceId");
document .getElementById("span").textContent = conferenceId;
fetch(`http://localhost:8080/getSubmission/${conferenceId}`)
  .then(response => response.json())
  .then(data => {
    let tableBody = document.getElementById('tableBody');
    data.forEach(obj => {
      let row = tableBody.insertRow();
      let paperIdCell = row.insertCell(0);
      let authorsCell = row.insertCell(1);
      let titleCell = row.insertCell(2);
      let reviewerCell = row.insertCell(3);
      let pdfCell = row.insertCell(4);
      paperIdCell.innerText = obj.publishPaperId;
      authorsCell.innerText = obj.authors.join(', ');
      titleCell.innerText = obj.title;
      if(obj.reviewer == null) { reviewerCell.innerText = "Not Assigned !"; }
      else { reviewerCell.innerText = obj.reviewer.email; }
      pdfCell.innerHTML = obj.reviewStatus;
      if(obj.reviewStatus == "NotReviewed") { pdfCell.style.color = 'blue';}
      else if(obj.reviewStatus == "Accepted") { pdfCell.style.color = 'green';}
      else { pdfCell.style.color = 'red';}
    });
  })
  .catch(error => {
    console.error('Error:', error);
  });

  let addReviewer = document.getElementById("addReviewer");

  addReviewer.onclick = ()=> {
      window.location.href = "http://localhost:8080/addreviewer";
  }

  let submissions = document.getElementById("submissions");

  submissions.onclick = ()=> {
      window.location.href = "http://localhost:8080/submissions/${conferenceId}";
  }