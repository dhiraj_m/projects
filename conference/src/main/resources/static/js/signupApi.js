function showLoginForm() {
  window.location.href = '/login';  // Redirect to the login page
}
var registrationForm = document.getElementById('registrationForm');

// Adding an event listener for the "submit" event
if (registrationForm) {
  registrationForm.addEventListener('submit', function (event) {
    // Prevent the default form submission behavior
    event.preventDefault();

    // Get the data from the input fields
    const firstName = document.getElementById('firstname').value;
    const lastName = document.getElementById('lastname').value;
    const email = document.getElementById('email_reg').value;
    const password = document.getElementById('password_reg').value;

    // URL of your API endpoint running locally
    const apiUrl = 'http://localhost:8080/signup'; // Replace with your API endpoint URL

    // Making a POST request to the API endpoint using Fetch API
    fetch(apiUrl, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ "firstName": firstName, "lastName": lastName, "email": email, "password": password }),
    })
      .then(response => {
        // Check if the request was successful
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
      })
      .then(data => {
        // Optionally, provide feedback to the user for a successful registration
        console.log('Registration successful:', data);
        // Redirect to the login page after registration
        showLoginForm();
      })
      .catch(error => {
        // Handle and provide feedback for errors during registration
        console.error('Registration error:', error);
      });
  });
}
