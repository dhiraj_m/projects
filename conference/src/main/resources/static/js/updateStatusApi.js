document.addEventListener('DOMContentLoaded', function() {
    document.getElementById('reviewUpdateForm').addEventListener('submit', function(event) {
        event.preventDefault(); // Prevent the form from submitting normally
        let publishPaperId = document.getElementById("number").value;
        let reviewStatus = document.getElementById("status").value;
        let f = document.getElementById("feedback").value;

        fetch(`http://localhost:8080/updateReviewStatus/${publishPaperId}/${reviewStatus}/${f}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => {
            if (!response.ok) {
                throw new Error(`HTTP error! Status: ${response.status}`);
            }
            return response.json();
        })
        .then(data => {
            console.log('Updated review status:', data);
                window.location.href = "/reviewer"

            // Handle the response data
        })
        .catch(error => {
            console.error('Error:', error);
        });

    });
});
